const express= require('express');
const server =express();

const swaggerJsDoc= require('swagger-jsdoc');
const swaggerUI= require('swagger-ui-express');


server.use(express.json());
server.use(express.urlencoded({extended: true}));


const swaggerOptions = {
   swaggerDefinition: {
     info: {
       title: 'Proyecto API.2',
       version: '1.0.0'
     }
   },
   apis: ['./swagger/swagger.js'],
};


const swaggerDocs = swaggerJsDoc(swaggerOptions);


server.use('/api-docs',swaggerUI.serve,swaggerUI.setup(swaggerDocs));


var usuarios = require('./rutas/usuarios');
server.use('/usuarios',usuarios);

var productos = require('./rutas/productos');
server.use('/productos', productos);

var pedidos = require('./rutas/pedidos');
server.use('/pedidos', pedidos);

var pagos = require('./rutas/pagos');
server.use('/pagos', pagos);

server.listen(3000, (req,res)=>{
    console.log('Escuchando en el puerto 3000')
  });