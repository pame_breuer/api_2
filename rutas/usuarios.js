const express= require('express');
var router =express.Router();

const usuarios = require('../models/usuarios');

const estaLog=  require('../middleware/adm_log');

const middleware = require('../middleware/usuarios')



router.get('/', function (req,res) {
    res.json({"usuarios":usuarios})
})


router.post('/',middleware.mailDup,middleware.esRequerido, function (req,res){
    const {nomb_usuario, nombre_apellido, pass, email, telefono, direccion} = req.body;

  const usuario = {
    id: usuarios.length +1,
    usuario: nomb_usuario,
    nombre_apellido: nombre_apellido,
    telefono: telefono,
    direccion: direccion,
    email: email,
    pass: pass,
    admin: false,
    logeado: false
  };

  usuarios.push(usuario);
  console.log('Usuario creado exitosamente');
  res.json({"Usuario":usuario});
  
});


router.post('/login', function (req,res) {

  const {email,pass}=req.body

  const enc= usuarios.find(x=>x.email === email && x.pass===pass);
  
  if (enc){
      enc.logeado=true;
      res.send('Usuario logueado');
      console.log('Usuario logueado')
      
  }else{
      res.send('Usuario o Email incorrectos');
  } 

});



router.put('/:id/',estaLog, function (req,res) {

  const {email,pass,usuario,nombre_apellido,telefono,direccion}=req.body

  const enc= usuarios.find(x=>x.email === email && x.pass===pass);
  
  if (enc){
      enc.usuario=usuario;
      enc.nombre_apellido=nombre_apellido;
      enc.telefono=telefono;
      enc.direccion=direccion;
      res.send('Usuario modificado');
  }else{
      res.send('Usuario o Email incorrectos');
  }
  
});



module.exports= router;