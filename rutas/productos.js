const express= require('express');
var router =express.Router();

const productos = require('../models/productos')

const valAdm=  require('../middleware/adm_log');
const estaLog=  require('../middleware/adm_log');
const validar_indice = require('../middleware/productos');


router.get('/', function (req,res) {
    res.json({"Productos":productos})
});


router.post('/',valAdm, estaLog, function (req,res) {

  const {nombre, precio} = req.body;

  const producto = {
    id_p: productos.length+1,
    nombre: nombre,
    precio: precio,
  };

  productos.push(producto);
  console.log('Producto creado exitosamente');
  res.json({"producto":producto});
    
});

router.put('/',valAdm, estaLog, function (req,res) {

    const {id,nombre,precio_nuevo,nombre_nuevo}=req.body;

    const enc= productos.find(x=> x.id===id && x.nombre===nombre);
    
    if (enc){
  
      enc.nombre=nombre_nuevo;
      enc.precio= precio_nuevo;
      res.send('Producto modificado exitosamente');
  
    }else{
  
      res.send('Nombre o id de producto incorrecto');
  
    };

});

router.delete('/',valAdm, estaLog, validar_indice, function (req,res) {

    productos.splice(req.body.indice,1)
    res.send('Producto eliminado')
   
});

module.exports= router;