const express= require('express');
var router =express.Router();

const pagos = require('../models/pagos')

const valAdm=  require('../middleware/adm_log');
const estaLog=  require('../middleware/adm_log');
const validar_indice = require('../middleware/pagos');



router.get('/', valAdm, estaLog, function (req,res) {
    res.json({"MediosDePago":pagos})
});


router.post('/', valAdm, estaLog, function (req,res) {

  const {nombre} = req.body;

  const pago = {
    id: pagos.length +1, 
    nombre: nombre,
  };

  pagos.push(pago);
  console.log('Medio de pago creado exitosamente');
  res.json({"Medios de pago":pago});
    
});



router.put('/',valAdm, estaLog , function (req,res) {

  const {nombre,nombre_nuevo}=req.body

  const enc= pagos.find(x=>x.nombre===nombre);
  
  if (enc){

    enc.nombre=nombre_nuevo;
    res.send('Medio de pago modificado exitosamente');

  }else{

    res.send('Nombre de medio de pago incorrecto');

  };
    
});


router.delete('/',valAdm, estaLog, validar_indice, function (req,res) {
  
  pagos.splice(req.body.indice,1);
  res.send('Pago eliminado');
 
});

module.exports= router;