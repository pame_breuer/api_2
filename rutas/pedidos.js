const express= require('express');
var router =express.Router();

const pedidos = require('../models/pedidos')
const productos = require('../models/productos')
const estados = require('../models/estados')
const pagos = require('../models/pagos')

const estaLog=  require('../middleware/adm_log');
const valAdm=  require('../middleware/adm_log');


router.get('/', valAdm, estaLog, function (req,res) {
    res.json({"Pedidos":pedidos})
})

router.post('/:id/', estaLog, function (req,res) {

  const { detalle,id_usuario,dire,metodo_pago} = req.body;

  let nomb="";
  let monto=0;

  detalle.forEach(detalle=> {
    productos.forEach(prod=>{
      if(prod.id==detalle.id){
        nomb = nomb + prod.nombre;
        monto= monto +  (prod.precio* detalle.cantidad);
      }
    })
  });

  pagos.forEach(pag=>{
  if(metodo_pago==pag.id){
    metodoPago= pag.nombre
  }
  })

  const nuevoPedido={
    id_pe: pedidos.length +1, 
    id_usuario,
    estado:"pendiente",
    nomb,
    monto,
    metodoPago,
    dire,
    detalle:detalle
    
  }

  pedidos.push(nuevoPedido);
  console.log('Pedido creado exitosamente');
  res.json({"Pedido nuevo":nuevoPedido});
    
});

router.get('/historial',estaLog, function (req,res) {

    const {id_usuario} = req.body;

    const historial =[];
  
    pedidos.forEach(pedido=>{
      if(pedido.id_usuario == id_usuario){
        historial.push(pedido)
      }
    });

    res.json({"Pedidos":historial})
});


router.put('/estado',valAdm,estaLog, function (req,res) {

    const {id_pe,ind}=req.body

    const enc= pedidos.find(x=>x.id_pe===id_pe);
    
    if (enc){
  
      enc.estado=estados[ind].nombre
      res.send('Estado modificado exitosamente');
  
    }  else{
      res.send('No se encontro el pedido')
    }
  
});


router.put('/', function (req,res) {
    res.send ('pedido editado')
})


module.exports= router;