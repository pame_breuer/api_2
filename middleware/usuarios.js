const usuarios = require('../models/usuarios');

const mailDup = (req, res , next) =>{

    const usuario =req.body;

    const bus = usuarios.find(x=> x.email ==usuario.email)

    if (bus){
        res.send ('El email ya fue utilizado');  
    }else{
        
        next();
    }
};

const esRequerido =  (req, res , next) =>{
      
    
        if(req.body.usuario !=="" && req.body.nombre_apellido !=="" && req.body.telefono !=="" && req.body.direccion !=="" &&req.body.email !=="" && req.body.pass !=="" ){
            next();
        }else{
            res.send("Falta completar campo")
        }
  
};

module.exports ={
    mailDup,
    esRequerido
}