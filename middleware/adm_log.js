const usuarios = require('../models/usuarios')

const valAdm = (req, res , next) =>{

    const {id_us} =req.body;

    const ad = usuarios.find(x=> x.id ===id_us && x.admin==true)

    if (ad){
        next();
    }else{
        res.send ('El usuario no es administrador')
    }

    
};

const estaLog = (req, res , next) =>{

    const {id_us} =req.body;

    const bus = usuarios.find(x=> x.id ==id_us && x.logeado==true)

    if (bus){
        next();
    }else{
        res.send ('El usuario no esta logeado')
    }

    
};

module.exports= valAdm;
module.exports= estaLog