const productos = require("../models/productos");

const validar_indice =(req,res,next)=>{
    if(req.body.indice >= 0 && req.body.indice < productos.length){
        next()
    }else{
        res.json({"indice":"invalido"})
    }
};

module.exports= validar_indice;