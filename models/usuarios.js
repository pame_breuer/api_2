const usuarios = [{
    id: 1,
    usuario: "nico_mag",
    nombre_apellido:'administrador',
    telefono: 654321,
    direccion: 'Mitre 1333',
    email: 'nicolas@gmail.com',
    pass: '123456',
    admin: true,
    logeado: false
  },
  {
    id: 2,
    usuario: "juanpe",
    nombre_apellido:'juan perez',
    telefono: 123456,
    direccion: 'San Martin 145',
    email: 'juanP@gmail.com',
    pass: '123456',
    admin: false,
    logeado: false
    }    
];

module.exports = usuarios;