let productos=[
    {
        id: 1,
        nombre: "Bagel de salmon",
        precio: 425
    },
    {
        id:2,
        nombre: "Hamburguesa clasica",
        precio: 350
    },
    {
        id:3,
        nombre: "Sandwich veggie",
        precio: 310
    },
    {
      id:4,
      nombre: "Ensalada veggie",
      precio: 340
    },
    {
      id:5,
      nombre: "Focaccia",
      precio: 300
    },
    {
      id:6,
      nombre: "Sandwich Focaccia",
      precio: 440
    },
];

module.exports = productos;