
//USUARIOS

/**
 * @swagger
 * /usuarios:
 *  get:
 *    description: Devuelve una lista de los usuarios
 *    responses:
 *      200:
 *        description: Success
 * 
*/


/**
 * @swagger
 * /usuarios:
 *  post:
 *    description: Crea un nuevo usuario
 *    parameters:
 *    - name: usuario
 *      description: nombre de usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: nombre_apellido
 *      description: Nombre y apellido del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: Telefono
 *      description: Telefono del usuario
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: Direccion
 *      description: Direccion del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: email
 *      description: Email del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: Pass
 *      description: contraseña del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
*/



/**
 * @swagger
 * /usuarios/login:
 *  post:
 *    description: Permite hacer login a un usuario
 *    parameters:
 *    - name: email
 *      description: email del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: pass
 *      description: pass del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
*/

//PRODUCTOS


/**
 * @swagger
 * /productos:
 *  get:
 *    description: Devuelve la lista de los productos
 *    responses:
 *      200:
 *        description: Success
 * 
*/


/**
 * @swagger
 * /productos:
 *  post:
 *    description: Crea un nuevo producto
 *    parameters:
 *    - name: nombre
 *      description: Nombre del nuevo producto
 *      in: formData
 *      required: true
 *      type: string
 *    - name: precio
 *      description: Precio del nuevo producto
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
*/



/**
 * @swagger
 * /productos:
 *  put:
 *    description: Modifica un producto
 *    parameters:
 *    - name: id_p
 *      description: Id del producto a modificar
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: nombre
 *      description: Nombre del producto 
 *      in: formData
 *      required: true
 *      type: string
 *    - name: precio
 *      description: Precio del producto 
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
*/



/**
 * @swagger
 * /productos:
 *  delete:
 *    description: Elimina un producto
 *    parameters:
 *    - name: id_p
 *      description: Id del producto a eliminar
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */

//MEDIOS DE PAGO

/**
 * @swagger
 * /pagos:
 *  get:
 *    description: Devuelve la lista de los medios de pago
 *    responses:
 *      200:
 *        description: Success
 * 
*/



/**
 * @swagger
 * /pagos:
 *  post:
 *    description: Crea un nuevo medio de pago
 *    parameters:
 *    - name: nombre
 *      description: nombre del medio de pago
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */

//editar medio de pago

/**
 * @swagger
 * /pagos:
 *  put:
 *    description: Modifica los medio de pago
 *    parameters:
 *    - name: id_mp
 *      description: Id del medio de pago a modificar
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: nombre
 *      description: nombre del medio de pago
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 * 
 */

//borrar medios de pago

/**
 * @swagger
 * /pagos:
 *  delete:
 *    description: Elimina un medio de pago
 *    parameters:
 *    - name: id_mp
 *      description: Id del medio de pago
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */

//PEDIDOS
//lista de pedidos

/**
 * @swagger
 * /pedidos:
 *  get:
 *    description: Devuelve la lista de los pedidos
 *    responses:
 *      200:
 *        description: Success
 * 
 */

//crear un nuevo pedido

/** 
* @swagger
 * /pedidos/:id/:
 *  post:
 *    description: Crear pedido nuevo
 *    consumes:
 *       - application/json
 *    produces:
 *       - application/json
 *    parameters:
 *    - in: body
 *      name: pedido
 *      description: pedido nuevo     
 *      schema:
 *                 type: object
 *                 required:
 *                   - datos
 *                 properties:
 *                   metodo_pago:
 *                     type: integer
 *                   id_us:
 *                     type: integer
 *                   detalle:
 *                     type: array
 *                     items:
 *                        type: object
 *                        properties:
 *                         id:
 *                           type: integer
 *                         cantidad:
 *                           type: integer 
 *    responses:
 *      200:
 *        description: Success
 * 
 */

//historial de pedidos!!!!

/**
 * @swagger
 * /pedidos/historial:
 *  get:
 *    description: Devuelve el historial de los pedidos
 *    parameters:
 *    - name: id
 *      description: Id del usuario
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */

//actualizar el pedido/ editar estado !!!

/**
 * @swagger
 * /pedidos/estado:
 *  put:
 *    description: Modifica el estado del pedido
 *    parameters:
 *    - name: id_pe
 *      description: Id del pedido
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: ind
 *      description: indice del nuevo estado del pedido
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */